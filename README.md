# Printer Driver Install Script

This is a basic Windows .bat script for installing a printer driver.

It uses the built in .vbs scripts provided by Microsoft to perform the actual installation of the drivers, creation of the printer port and creation of the printer.

In this example we are installing the Canon Generic Plus PCL6 driver. This driver performs bidirectional communications with the printer during the installation in order to setup the correct model as well as set the finishing functions. Because of this the script first checks that the printer is available on the network and does not proceed if it does not respond to a ping.

User will need permissions to install drivers.

You will need to edit the script to adjust the parameters appropriate for your site and where the drivers are located.

