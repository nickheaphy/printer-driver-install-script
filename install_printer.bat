@ECHO OFF
SETLOCAL

REM Before using this script you will need to download the printer driver that you
REM want to install. This will consist of a number of files, but you need to know
REM the path to the .inf file, along with the name of the printer driver that can 
REM be found in the .inf. This information needs to be included in the script below

REM Change these variable to what is appropate for your install

REM What should the printer name be as presented to the user in the print dialog?
SET printername=Canon Printer

REM What is the IP/Hostname of the printer?
SET printerip=10.10.2.56

REM The path to the scripts can be different depending on the version of Windows.
REM This is the path for Windows 10
SET pathtoscripts=%windir%\system32\printing_admin_scripts\en-us

REM You will need to adjust the following based on where the drivers actually are
REM and the name of the printer driver inside the .inf file
REM Here we are installing the Canon Generic Plus PCL Driver from a folder in the same
REM directory as where the script is running.
SET printerdrivername=Canon Generic Plus PCL6
SET pathtodriverinf=%~dp0GPlus_PCL6_Driver_V230_W64_00\Driver\CNP60MA64.INF


REM First lets ping the printer to make sure it is online. We are not going to do the install
REM unless online (as the Generic driver does a bidirectional comms to setup the finisher etc). If you
REM install without connection to the printer, once connected you will need to update using the
REM Device Settings tab in the printer properties.

PING -n 1 %printerip% | FIND "TTL=" > nul
IF ERRORLEVEL 1 (
    ECHO Printer is not online. Please try again when printer available
) ELSE (
    REM The first Prnmngr.vbs is to delete the printer if you're trying to add a printer with the same name
    REM Supresses the errors that will inevitably occur if this is the first install
    CSCRIPT //Nologo "%pathtoscripts%\Prnmngr.vbs" -d -p "%printername%" > nul 2> nul

    REM Adds a printer port with name "IP_printerip" and IP address of "printerip"
    CSCRIPT //Nologo "%pathtoscripts%\Prnport.vbs" -a -r IP_%printerip% -h %printerip% -o raw -n 9100

    REM Installs the printer driver
    CSCRIPT //Nologo "%pathtoscripts%\Prndrvr.vbs" -a -m "%printerdrivername%" -i "%pathtodriverinf% "

    REM Finally give the printer a name and link the model and printer port
    CSCRIPT //Nologo "%pathtoscripts%\Prnmngr.vbs" -a -p "%printername%" -m "%printerdrivername%" -r IP_%printerip%
)

ENDLOCAL
@pause